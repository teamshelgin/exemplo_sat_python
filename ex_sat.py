import ctypes

#Carrega biblioteca SAT
_lib = ctypes.cdll.LoadLibrary("C:\Program Files (x86)\satelgin 5.0.4\lib\dllsat.dll")

#função para apresentar o restorno do SAT na Console
def show_return(str1):
    size = -1
    str1 = ctypes.string_at(str1,size)
    print(str1.decode('utf-8')) 

atv = '123456789'.encode('utf-8') #Codigo de ativação padrão SAT KIT DEV
atv_teste = '987654321'.encode('utf-8') #Codigo de ativação para teste da função TrocarCodigoDeAtivacao
cnpj_sh = '16716114000172'.encode('utf-8') #CNPJ software house
cnpj_ct = '14200166000166'.encode('utf-8') #CNPJ contribuinte
assinatura_sh = 'SGR_SAT SISTEMA DE GESTAO E RETAGUARDA DO SAT'.encode('utf-8')

#Carrega o XML de teste fim a fim
arquivo = open('teste_fim_a_fim.xml', 'r', encoding="utf8")
fim_a_fim = arquivo.read().encode('utf-8')
arquivo.close

#Carrega XML de venda 
arquivo = open('XML_Venda.xml', 'r', encoding="utf8")
xml_venda = arquivo.read().encode('utf-8')
arquivo.close

#Carrega XML de cancelamento 
arquivo = open('XML_cancelamento.xml', 'r', encoding="utf8")
xml_cancelamento = arquivo.read()
arquivo.close

#Carrega XML de configuração de rede
arquivo = open('conf_rede.xml', 'r', encoding="utf8")
xml_conf_rede = arquivo.read().encode('utf-8')
arquivo.close


#=================== Funções
# Função da dllsat Elgin para geração de numeros de sessão
# int GeraNumeroSessao()
ret = _lib.GeraNumeroSessao()
print("Numero sessao gerado: ", ret)
sessao = _lib.GeraNumeroSessao #Atribuição da função a um objeto para facilitar uso

#===================
# Função dllsat Elgin para a ativação do equipamento SAT
# char *AtivarSAT(int numeroSessao, int subComando, char *codigoAtivacao, char *CNPJ, int cUF)
str0 = _lib.AtivarSAT(sessao(), 1, atv, cnpj_ct, 35)
show_return(str0)

#===================
# Função da dllsat Elgin para validar comunicação entre o PC e o SAT
# char *ConsultarSAT(int)
str0 = _lib.ConsultarSAT(sessao())
show_return(str0)

#===================
# Função da dllsat Elgin para realizar teste de comunicação com a sefaz
# char *TesteFimAFim(int numeroSessao, char *codigoDeAtivacao, char *dadosVenda)
str0 = _lib.TesteFimAFim(sessao(), atv, fim_a_fim)
show_return(str0)

#===================
# Função da dllsat Elgin para consultar as configurações do SAT
# char *ConsultarStatusOperacional(int numeroSessao, char *codigoDeAtivacao)
str0 = _lib.ConsultarStatusOperacional(sessao(), atv)
show_return(str0)

#===================
# Função da sllsat Elgin para efetuar venda 
# char *EnviarDadosVenda(int numeroSessao, char *codigoDeAtivacao, char *dadosVenda)
str0 = _lib.EnviarDadosVenda(sessao(), atv, xml_venda)
show_return(str0)

#Captura chave de cancelamento para uso da função CancelarUltimaVenda
size2 =-1
str2 = ctypes.string_at(str0,size2)
str2 = str2.decode('utf-8').split('|')[8]
xml_cancelamento = xml_cancelamento.replace("_&_", str2)

#===================    
# Função da dllsat Elgin para realizar cancelamento de notas
# char *CancelarUltimaVenda(int numeroSessao, char *codigoDeAtivacao, char *chave, char *dadosCancelamento)
str0 = _lib.CancelarUltimaVenda(sessao(), atv, str2.encode('utf-8'), xml_cancelamento.encode('utf-8'))
show_return(str0)

#===================    
# Função da dllsat Elgin para realizar consulta ao ultimo numero de sessao processado
# char *ConsultarNumeroSessao(int numeroSessao, char *codigoDeAtivacao, int cNumeroDeSessao)
mSessao = sessao() #Guarda um numero de sessao para o teste
str0 = _lib.ConsultarSAT(mSessao) #Invova função consulta passando o numero de sessao armazenado anteriormente
show_return(str0)
str0 = _lib.ConsultarNumeroSessao(sessao(), atv, mSessao)#Consulta a sessao processada anteriormente
show_return(str0)

#===================
# Função da dllsat Elgin para associar a assinatura da software house
# char *ConfigurarInterfaceDeRede(int numeroSessao, char *codigoDeAtivacao, char *dadosConfiguracao)
str0 = _lib.ConfigurarInterfaceDeRede(sessao(), atv, xml_conf_rede)
show_return(str0)  

#===================    
# Função da dllsat Elgin para associar a assinatura da software house
# char *AssociarAssinatura(int numeroSessao, char *codigoDeAtivacao, char *CNPJvalue, char *assinaturaCNPJs)
str0 = _lib.AssociarAssinatura(sessao(), atv, cnpj_sh, assinatura_sh)
show_return(str0)    

#===================
# Função da dllsat Elgin para atualização do software do sat
# char *AtualizarSoftwareSAT(int numeroSessao, char *codigoDeAtivacao)
str0 = _lib.AtualizarSoftwareSAT(sessao(), atv)
show_return(str0) 

#===================
# Função da dllsat Elgin para extrair logs
# char *ExtrairLogs(int numeroSessao, char *codigoDeAtivacao)
str0 = _lib.ExtrairLogs(sessao(), atv)
show_return(str0) 

#===================
# Função da dllsat Elgin para bloqueio do sat
# char *BloquearSAT(int numeroSessao, char *codigoDeAtivacao)
str0 = _lib.BloquearSAT(sessao(), atv)
show_return(str0) 

#===================    
# Função da dllsat Elgin para desbloqueio do sat
# char *DesbloquearSAT(int numeroSessao, char *codigoDeAtivacao)
str0 = _lib.DesbloquearSAT(sessao(), atv)
show_return(str0) 

#===================
# Função da dllsat Elgin para troca do codigo de ativação do sat
# char *TrocarCodigoDeAtivacao(int numeroSessao, char *codigoDeAtivacao, int opcao, char *novoCodigo, char *confNovoCodigo)
str0 = _lib.TrocarCodigoDeAtivacao(sessao(), atv, 1, atv_teste, atv_teste)
show_return(str0)    
#Alteração para o valor padrão
str0 = _lib.TrocarCodigoDeAtivacao(sessao(), atv_teste, 1, atv, atv)
show_return(str0)    

#===================
# Função da dllsat Elgin para consulta da ultima operação fiscal, venda ou cancelamento
# char *ConsultarUltimaSessaoFiscal(int numeroSessao, char *codigoDeAtivacao)
str0 = _lib.ConsultarUltimaSessaoFiscal(sessao(), atv)
show_return(str0)
